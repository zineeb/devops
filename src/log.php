<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload

use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Maxbanton\Cwh\Handler\CloudWatch;
use Monolog\Logger;
use Monolog\Formatter\JsonFormatter;

$sdkParams = [
    'region' => getenv('AWS_REGION'),
    'version' => 'latest',
    'credentials' => [
        'key' => getenv('AWS_KEY'),
        'secret' => getenv('AWS_SECRET')
    ]
];

$client = new CloudWatchLogsClient($sdkParams);
$groupName = 'php-logtest-mouazzaz';
$streamName = 'ecs-stream-ynov';
$retentionDays = 30;
try {
    $handler = new CloudWatch($client, $groupName, $streamName, $retentionDays, 10000, ['my-awesome-tag' => 'tag-value']);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$handler->setFormatter(new JsonFormatter());

$log = new Logger('name');
$log->pushHandler($handler);

function addLog($level,$message,$fileName): void
{
    global $log;

    $formattedMessage = sprintf("[%s] %s", $fileName, $message);

    switch ($level) {
        case 'DEBUG':
            $log->debug($formattedMessage);
            break;
        case 'INFO':
            $log->info($formattedMessage);
            break;
        case 'WARNING':
            $log->warning($formattedMessage);
            break;
        case 'ERROR':
            $log->error($formattedMessage);
            break;
    }
}