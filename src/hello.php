<?php

require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload
require_once 'log.php';

$fileName = basename(__FILE__);

use HelloWorld\SayHello;

echo SayHello::world();
addLog('INFO', 'Appel de la page hello', $fileName);
